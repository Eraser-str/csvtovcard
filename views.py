# -*- coding: utf-8 -*-
from flask import render_template, request, redirect, url_for, flash
from werkzeug.utils import secure_filename

from config import app, csv_temp_dir
from models import csv2vcard, str_gen


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/csv/upload/', methods=['POST'])
def csv_upload():
    """Представление получает файл и сохраняет его, после чего вызывает функцию конвертации

    """
    f = request.files['csvfile']
    if f.filename.split('.')[-1:][0] != 'csv':
        return "Поддерживаются файлы только формата .csv"
    filename = str_gen(16) + '.csv'
    filepath = csv_temp_dir + '/' + filename
    f.save(filepath)
    csv2vcard(filename)
    return render_template('done.html')