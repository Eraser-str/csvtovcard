# -*- coding: utf-8 -*-
import csv
import random
from string import letters, hexdigits

import vobject

from config import vcard_dir, csv_temp_dir


def str_gen(length):
    """Генератор случайной строки. Используется для названий файлов, загружаемых пользователями

    :param length: длина строки

    :type length: int

    :rtype: str

    """
    return "".join(random.choice(letters + hexdigits) for _ in range(length))


def csv2vcard(csv_filename):
    """Функция, конвертирующая .csv файл в набор карточек vcard. Задейсвует модули csv и vobject

    :param csv_filename: имя csv файла, из которого необходимо сделать vcard

    :type csv_filename: str

    :rtype: bool

    """
    with open(csv_temp_dir + '/' + csv_filename, 'rb') as raw_file:
        raw_data = csv.DictReader(raw_file)

        for user in raw_data:
            # Собираем ключи с пустыми значениями и удаляем их
            empty_keys = [k for k,v in user.iteritems() if not v]
            for k in empty_keys:
                del user[k]

            # Если строка не является юникодом, попробуем декодировать ее как windows-1251 или ascii.
            # iteritems() возвращает кортеж, поэтому k - ключ (key) из словаря user, а v - значение (value)
            if isinstance(user['First Name'], unicode) is False:
                for k, v in user.iteritems():
                    try:
                        user[k] = v.decode("windows-1251", "replace")
                    except UnicodeDecodeError:
                        try:
                            user[k] = v.decode("ascii", "replace")
                        except:
                            break

            # Создаем карточку
            if 'Display Name' in user:
                card = vobject.vCard()
                attr = card.add('fn')
                attr.value = user['Display Name']
                if 'First Name' and 'Last Name' in user:
                    attr = card.add('n')
                    attr.value = vobject.vcard.Name(family=user['Last Name'], given=user['First Name'])
                if 'Nickname' in user:
                    attr = card.add('nickname').value = user['Nickname']
                if 'Birth Year' in user \
                    and 'Birth Month' in user \
                    and 'Birth Day' in user:
                        attr = card.add('bday')
                        attr.value = user['Birth Year'] + '-' + user['Birth Month'] + '-' + user['Birth Day']
                if 'Work Phone' in user:
                    attr = card.add('tel')
                    attr.type_param = 'work'
                    attr.value = user['Work Phone']
                if 'Home Phone' in user:
                    attr = card.add('tel')
                    attr.type_param = 'home'
                    attr.value = user['Home Phone']
                if 'Fax Phone' in user:
                    attr = card.add('fax')
                    attr.type_param = 'work'
                    attr.value = user['Fax Phone']
                if 'Primary Email' in user:
                    attr = card.add('email')
                    attr.type_param = 'pref'
                    attr.value = user['Primary Email']
                # etc...

                # Называем и сохраняем сконвертированный файл
                vcf_filename = csv_filename.split('.')[0] + '.vcf'
                with open(vcard_dir + '/' + vcf_filename, 'a+') as f:
                    f.write(card.serialize() + '\n')
    return True